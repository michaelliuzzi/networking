import numpy as np
import matplotlib.pyplot as plt
import pdb

rtt_list_mitm = []
rtt_list = []

with open("graph_three_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list_mitm.append(float(rtt))


with open("graph_one_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list.append(float(rtt))



plt.plot(np.array(rtt_list))
plt.plot(np.array(rtt_list_mitm))
plt.legend(["Non MITM", "MITM"])
plt.title("MITM vs Non MITM RTT")

plt.show()




