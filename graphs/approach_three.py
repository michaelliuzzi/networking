import numpy as np
import matplotlib.pyplot as plt
import pdb

rtt_list_mitm = []
rtt_list = []
thresh_list = []

AVERAGE = 0
THRESH = 1.069
with open("graph_three_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list_mitm.append(float(rtt))


with open("graph_one_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")

        rtt_list.append(float(rtt))
        thresh_list.append(float(threshold))


false_negative = 0
total = 0
for pair in zip(rtt_list_mitm, thresh_list):
    total = total + 1
    if pair[0] < pair[1]:
        false_negative = false_negative + 1

false_positive = 0
for pair in zip(rtt_list, thresh_list):
    if pair[0] > pair[1]:
        false_positive = false_positive + 1


print(false_negative / total)
print(false_positive / total)
        

plt.plot(np.array(rtt_list_mitm))
plt.plot(np.array(rtt_list))
plt.plot(np.array(thresh_list))
plt.legend(["RTT MITM", "Non MITM RTT",  "Threshold"], loc="upper center")
plt.title("Approach three")
plt.show()

