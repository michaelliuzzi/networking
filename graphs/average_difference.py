import numpy as np
import matplotlib.pyplot as plt
import pdb

rtt_list_mitm = []
rtt_list = []

with open("graph_three_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list_mitm.append(float(rtt))


with open("graph_one_data.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list.append(float(rtt))

average = 0
pdb.set_trace()
for pair in zip(rtt_list_mitm, rtt_list):
    diff = pair[0] / pair[1]
    average = (diff + average) / 2

print(average)
