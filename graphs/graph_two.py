import numpy as np
import matplotlib.pyplot as plt
import pdb

#MITM Data!

#read in data
#rtt, average, threshold
#Min value for threshold = highest value for rtt
#However that might not be sufficient to detect majority of MITM attacks.
#Discuss tradeoff 
#Do a second analysis where we increase the time between get requests to see if it reduces variance and eliminates possibility of virtual machine as a bottleneck.


rtt_list = []
average_list = []
pure_average_list = []
threshold_list = []
max_rtt_list = []

MAX_RTT = 0
AVERAGE = 0
with open("graph_two_data2.txt") as f:
    for line in f:
        rtt, average, threshold = line.split(",")
        rtt_list.append(rtt)
        average_list.append(average)
        threshold_list.append(threshold)
        if not pure_average_list:
            pure_average_list.append(float(rtt_list[0]))
            AVERAGE = float(rtt_list[0])
        else:
            AVERAGE = (AVERAGE + float(rtt)) / 2
            pure_average_list.append(AVERAGE)

        if float(rtt) > MAX_RTT:
            MAX_RTT = float(rtt)

for val in threshold_list:
    max_rtt_list.append(MAX_RTT)

plt.plot(np.array(rtt_list))
plt.plot(np.array(average_list))
plt.plot(np.array(threshold_list))
plt.plot(np.array(max_rtt_list))
plt.plot(np.array(pure_average_list))
plt.legend(["RTT","Filtered Average","Threshold", "Zero FP Thresh", "Pure Average"], loc="upper center")
plt.title("Requests at two second interval - Non MITM")
plt.show()


