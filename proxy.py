#MITM Proxy
#Single threaded proxy
#Use Scapy to intercept traffic flows, record RTT, and make decisions.
#Python 2.7
#Michael Liuzzi

import socket
import pdb
from scapy.all import *
from scapy.layers import http
import re
import time
import sqlite3

HOST = "192.168.1.8:31990"
NUM = 0
class MITMException(Exception):
    def __init__(self, delta, average):
        self.delta = delta
        self.average  = average
    

class Proxy():
    def __init__(self, thresh=100):
         self.state = {}
         self.averages = {}
         self.thresh = thresh
         self.conn = sqlite3.connect('domains.db')
         self.c = self.conn.cursor()
         self.c.execute("CREATE TABLE IF NOT EXISTS domains (domain TEXT, average NUM)")
         self.c.execute("SELECT domain, average FROM domains")
         result = self.c.fetchall()
         for pair in result:
             self.averages[pair[0]] = pair[1]
         self.export = open("export.txt", "w")
         f = open("MITM.html")
         self.mitm_html = f.read()
    def run(self):
        try:
            sock_listen = socket.socket()
            sock_listen.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock_listen.bind(("localhost", 31990))
            sock_listen.listen(100)
            while True:
                (sock_accept, addr) = sock_listen.accept()
                self.handle(sock_accept, addr)
        except KeyboardInterrupt:
            self.cleanup()
            sys.exit(0)

    def cleanup(self):
        for domain in self.averages:
            self.c.execute("SELECT * FROM domains WHERE domain = ?", (domain,))
            exists = self.c.fetchone()
            if not exists:
                self.c.execute("INSERT INTO domains (domain, average) VALUES (?,?)", (domain, self.averages[domain] ))
            else:
                self.c.execute("UPDATE domains SET average = ? WHERE domain = ?", (self.averages[domain], domain))

        self.conn.commit()
        self.conn.close()
        self.export.close()
        
            
    def handle(self, sock, addr):
        #read/write loop for a single connection
        application_read_buff = sock.recv(2048)
        packet = http.HTTP(application_read_buff)
        try:
            if str(packet.Method) != "GET":
                return
            address, port = str(packet.Host).split(":")
        except:
            return
        #replace absolute url with resource path
        path = str(packet.Path)
        match = re.search(":\d+/", path)
        resource  = path[match.end() - 1:]
        packet.Path = resource
        port = int(port)
        sock_con = socket.socket()
        sock_con.connect((address, port))
        self.do_bookkeeping(packet, 0)
        sock_con.send(str(packet))
        
        server_recv_buf = sock_con.recv(2048)
        
        try:
            self.do_bookkeeping(packet, 1)
        except MITMException as e:
            print("MITM ATTACK DETECTED")
            print("Delta: " + str(e.delta) )
            print("Average: " + str(e.average) )
            print("Aborting connection....")
            sock.send(self.mitm_html)
            return
            
  
            sock.send(server_recv_buf)

    def do_bookkeeping(self, packet, send_recv_flag):

        if send_recv_flag == 0:
            try:
                self.state[str(packet.Host)].append([time.clock()])
            except:
                self.state[str(packet.Host)] = [[time.clock()]]
        if send_recv_flag == 1:
            latest_connection = len(self.state[str(packet.Host)]) - 1
            self.state[str(packet.Host)][latest_connection].append(time.clock())
            self.update_averages(packet)
            

    
    def update_averages(self, packet):
        latest_connection = len(self.state[str(packet.Host)]) - 1
        pair =  self.state[str(packet.Host)][latest_connection]
        delta = pair[1] - pair[0]

        try:
            print("Delta:" + str(delta))
            print("Average:" + str(self.averages[str(packet.Host)]))
            if delta > self.thresh * self.averages[str(packet.Host)]:
                #Add data that we consider a MITM attack as well!
                if str(packet.Host) == HOST:
                    self.export_delta(delta, self.averages[str(packet.Host)], self.averages[str(packet.Host)] * self.thresh)

                raise MITMException(delta, self.averages[str(packet.Host)])
            
            else:
                self.averages[str(packet.Host)] = (delta + self.averages[str(packet.Host)]) / 2
        except KeyError:
            self.averages[str(packet.Host)] = delta

        #Data we do not consider MITM attack
        if str(packet.Host) == HOST:
            self.export_delta(delta, self.averages[str(packet.Host)], self.averages[str(packet.Host)] * self.thresh )


    #write RTT, average, threshold to newline separated file
    #compute an average of the threshold values
    def export_delta(self, delta, average, threshold):
        #global NUM
        #NUM = NUM + 1
        #print(NUM)
        self.export.write(str(delta)+ "," + str(average) + "," + str(threshold) + "\n")
        
        
        
        
    


if __name__ == "__main__":
    #Parse arguments and instantiate proxy
    proxy_instance = Proxy()
    proxy_instance.run()



    
    
OB
